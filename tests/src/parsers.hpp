#pragma once

#include <gtest/gtest.h>
#include <map_parser.hpp>
#include <req_parser.hpp>
#include <request.hpp>
#include <string>

htcpp::parse_result req_tester(std::string src)
{
    htcpp::parser parser;
    htcpp::request dummy_;

    return parser.parse(dummy_, src.begin(), src.end());
}

TEST(RequestParsing, Successes)
{

    ASSERT_EQ(req_tester("GET /a/resource/ HTTP/1.1\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_tester("POST /a/resource/ HTTP/1.1\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_tester("PUT /a/resource/ HTTP/1.1\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_tester("OPTIONS /a/resource/ HTTP/1.1\r\n\r\n"), htcpp::parse_result::ok);
}

TEST(RequestParsing, MapSuccesses)
{

    ASSERT_EQ(req_tester("GET /a/resource/ HTTP/1.1\r\naKey: aValue\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_tester("POST /a/resource/ HTTP/1.1\r\nk: v\r\nk: v\r\n\r\n"), htcpp::parse_result::ok);
}

TEST(RequestParsing, Failures)
{
    ASSERT_EQ(req_tester("get /a/resource/ HTTP/1.1\\n"), htcpp::parse_result::error);
    ASSERT_EQ(req_tester("Set /a/resource/ HTTP/1.1\r\n"), htcpp::parse_result::error);
    ASSERT_EQ(req_tester("/a/resource/ HTTP/1.1\r\n"), htcpp::parse_result::error);
    ASSERT_EQ(req_tester("PUT HTTP/1.1\r\n"), htcpp::parse_result::error);
}

TEST(RequestParsing, Undefined)
{
    ASSERT_EQ(req_tester("GET /a/resource/"), htcpp::parse_result::undefined);
    ASSERT_EQ(req_tester("SET"), htcpp::parse_result::undefined);
    ASSERT_EQ(req_tester("PUT /a/resource/ HTTP/1.1"), htcpp::parse_result::undefined);
    ASSERT_EQ(req_tester("OPTIONS /a/resource/ HTTP"), htcpp::parse_result::undefined);
}

TEST(MapParsing, Success)
{
    auto req_map = [](std::string src) {
        htcpp::map_parser parser = htcpp::map_parser::request();
        std::unordered_map<std::string, std::string> dummy_;

        return parser.parse(dummy_, src.begin(), src.end());
    };

    auto config_map = [](std::string src) {
        htcpp::map_parser parser = htcpp::map_parser::config();
        std::unordered_map<std::string, std::string> dummy_;

        return parser.parse(dummy_, src.begin(), src.end());
    };

    ASSERT_EQ(req_map("\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_map("aKey: aVal\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_map("aKey: aVal\r\noKey: oVal\r\n\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(req_map("aKey : aVal\r\noKey :    oVal\r\n\r\n"), htcpp::parse_result::ok);

    // ASSERT_EQ(config_map("\r\n"), htcpp::parse_result::ok);
    ASSERT_EQ(config_map("aKey: aVal\n"), htcpp::parse_result::ok);
    ASSERT_EQ(config_map("aKey: aVal\n\nok : val\n\n"), htcpp::parse_result::ok);
    ASSERT_EQ(config_map("\n\n aKey: aVal\n\nok : val\n\n"), htcpp::parse_result::ok);
    ASSERT_EQ(config_map("aKey: aVal\noKey: oVal\n"), htcpp::parse_result::ok);
}
