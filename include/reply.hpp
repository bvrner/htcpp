#pragma once

#include <asio.hpp>
#include <string>
#include <vector>

namespace htcpp {

struct reply {

    enum class status {
        ok,
        not_found,
        bad
    } status_;

    struct header {
        std::string key;
        std::string value;
    };

    std::vector<header> headers;
    std::string body;

    static reply common_reply(status);
    std::vector<asio::const_buffer> to_buffers();
};

}
