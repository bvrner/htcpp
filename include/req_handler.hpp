#pragma once

#include <filesystem>
#include <fstream>
#include <request.hpp>
#include <sstream>
#include <string>
#include <string_view>

#include <reply.hpp>

namespace htcpp {

class req_handler {
    // req_handler();
    req_handler(req_handler&) = delete;

    std::filesystem::path root_;

    static bool get_file_contents(std::filesystem::path& path, std::string& contents)
    {
        std::ifstream in(path, std::ios::in | std::ios::binary);
        if (in) {
            in.seekg(0, std::ios::end);
            contents.resize(in.tellg());
            in.seekg(0, std::ios::beg);
            in.read(&contents[0], contents.size());
            in.close();
            return true;
        }

        return false;
    }

    [[nodiscard]] reply get(request& req);
    [[nodiscard]] reply head(request& req);

public:
    req_handler(const std::string root)
        : root_(root)
    {
    }

    [[nodiscard]] reply handle(request& req)
    {
        switch (req.method_) {
        case request::method::get:
            return get(req);
        case request::method::head:
            return head(req);
        default:
            return reply::common_reply(reply::status::bad);
        }
    }
};
}
