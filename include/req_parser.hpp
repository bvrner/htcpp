#pragma once

#include <map_parser.hpp>
#include <parser.hpp>
#include <request.hpp>
#include <string>

namespace htcpp {

// a request parser
// it's basically the same as the one in Asio's C++11 examples, but in my way to write it
class parser {
public:
    parser()
        : state_(state::method_begin)
        , map_parser_(htcpp::map_parser::request())
    {
    }
    void reset()
    {
        state_ = state::method_begin;
        buf_.clear();

        map_parser_.reset();
    }

    template <typename InputIterator>
    htcpp::parse_result parse(htcpp::request& req, InputIterator begin, InputIterator end)
    {
        for (; begin != end; begin++) {
            if (state_ == state::map) {
                return map_parser_.parse(req.headers, begin, end);
            } else {
                auto res = consume(req, *begin);
                if (res != htcpp::parse_result::undefined) {
                    return res;
                }
            }
        }
        return htcpp::parse_result::undefined;
    }

private:
    enum class state {
        method_begin,
        method,
        resource,
        version_h,
        version_t_1,
        version_t_2,
        version_p,
        version_slash,
        version_major,
        version_minor,
        expecting_newline_req,
        // expecting_return,
        map
    } state_;

    // general use buffer to store intermediate representations
    std::string buf_;
    htcpp::map_parser map_parser_;

    htcpp::parse_result consume(htcpp::request&, const char);
};
}
