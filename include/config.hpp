#pragma once

#include <filesystem>
#include <fstream>
#include <iostream>
#include <map_parser.hpp>
#include <stdexcept>
#include <unordered_map>

namespace htcpp {

struct config {
    unsigned int port;
    std::filesystem::path root;
    std::size_t threads;

    config()
        : port(5000)
        , root("./www")
        , threads(1)
    {
    }
    config(std::filesystem::path& file)
    {
        std::ifstream in(file, std::ios::in);
        if (in) {
            std::string contents;
            std::unordered_map<std::string, std::string> content_map;

            map_parser parser = map_parser::config();

            in.seekg(0, std::ios::end);
            contents.resize(in.tellg());
            in.seekg(0, std::ios::beg);
            in.read(&contents[0], contents.size());
            in.close();

            auto res = parser.parse(content_map, contents.begin(), contents.end());

            if (res != parse_result::ok) {
                // maybe I should just return a default instead?
                // TODO report the mistake maybe?
                throw std::runtime_error("Couldn't parse config file");
            }

            // yes yes, this sucks, but it's an ad hoc
            // and I really do miss Rust's unwrap_or in these moments
            auto port_loc = content_map.find("port");
            auto root_loc = content_map.find("root");
            auto threads_loc = content_map.find("threads");

            if (port_loc != content_map.end()) {
                port = std::stoi(port_loc->second);
            } else {
                port = 5000;
            }

            if (root_loc != content_map.end()) {
                root = std::filesystem::path(root_loc->second);
            } else {
                root = std::filesystem::current_path();
            }

            if (threads_loc != content_map.end()) {
                threads = std::stoi(threads_loc->second);
            } else {
                threads = 1;
            }
        }
    }
};
}
