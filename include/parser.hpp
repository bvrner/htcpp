#pragma once

namespace htcpp {

enum class parse_result {
    ok,
    error,
    undefined
};
}

// // a request parser
// // it's basically the same as the one in Asio's C++11 examples, but in my way to write it
// class parser {
// public:
//     parser()
//         : state_(state::method_begin)
//     {
//     }
//     void reset()
//     {
//         state_ = state::method_begin;
//         buf_.clear();
//     }

//     template <typename InputIterator>
//     parse_result parse(htcpp::request& req, InputIterator begin, InputIterator end)
//     {
//         for (; begin != end; begin++) {
//             auto res = consume(req, *begin);

//             if (res != parse_result::undefined) {
//                 return res;
//             }
//         }

//         return parse_result::undefined;
//     }

// private:
//     enum class state {
//         method_begin,
//         method,
//         resource,
//         version_h,
//         version_t_1,
//         version_t_2,
//         version_p,
//         version_slash,
//         version_major,
//         version_minor,
//         expecting_newline_req,
//         expecting_newline_body,
//         expecting_return,
//         body_val_begin,
//         body_val,
//         body_dot,
//         body_key,
//         newline_end
//     } state_;

//     // general use buffer to store intermediate representations
//     std::string buf_;

//     parse_result consume(htcpp::request&, const char);
// };
// }
