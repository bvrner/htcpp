#pragma once

#include <parser.hpp>
#include <string>
#include <unordered_map>

namespace htcpp {

// both the http requests and the config file will use the same key-map scheme except for how whitespace is handled
// so I decide to separate a class of it's own to parse it, with defined rules for both, the ideia is the same of the request parser
class map_parser {
public:
    static map_parser config()
    {
        return map_parser(kind::config);
    }
    static map_parser request()
    {
        return map_parser(kind::request);
    }

    void reset()
    {
        key_buf_.clear();
        val_buf_.clear();
        state_ = state::key_begin;
    }

    template <typename InputIterator>
    htcpp::parse_result parse(std::unordered_map<std::string, std::string>& map, InputIterator begin, InputIterator end)
    {
        for (; begin != end; begin++) {
            auto res = consume(map, *begin);

            if (res != htcpp::parse_result::undefined) {
                return res;
            }
        }

        return kind_ == kind::request ? htcpp::parse_result::undefined : htcpp::parse_result::ok;
    }

private:
    enum class kind {
        config,
        request
    } kind_;

    enum class state {
        key_begin,
        key,
        colon,
        value_begin,
        value,

        expect_newline,
        expect_newline_end
    } state_;

    map_parser(kind kind)
        : kind_(kind)
        , state_(state::key_begin)
    {
    }

    std::string key_buf_;
    std::string val_buf_;
    htcpp::parse_result consume(std::unordered_map<std::string, std::string>& map, const char c);
};
}
