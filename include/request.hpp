#pragma once

#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace htcpp {
struct request {

    enum class method {
        get,
        post,
        put,
        head,
        del,
        options,
        bad
    };

    // a table of string equivalents to the methods
    // static std::unordered_map<std::string, method> const table;

    method method_;
    std::string resource;
    int major;
    int minor;
    std::unordered_map<std::string, std::string> headers;

    request()
        : method_(method::bad)
        , major(1)
        , minor(1)
    {
    }
};
}
