#pragma once

#include <asio.hpp>
#include <config.hpp>
#include <connection.hpp>
#include <req_handler.hpp>
#include <thread>
#include <vector>

using asio::ip::tcp;
namespace htcpp {
class server {

public:
    server(config& options)
        : io_context_()
        , acceptor_(io_context_, tcp::endpoint(tcp::v4(), options.port))
        , handler_(options.root)
    {
        start_acceptor();
    }

    void run(std::size_t n_threads)
    {
        std::vector<std::thread> threads;
        threads.reserve(n_threads - 1);

        for (auto i = n_threads - 1; i > 0; i--)
            threads.emplace_back([this] { this->io_context_.run(); });

        io_context_.run();

        for (auto& t : threads)
            t.join();
    }

private:
    asio::io_context io_context_;
    tcp::acceptor acceptor_;
    req_handler handler_;

    void start_acceptor();
};

}
