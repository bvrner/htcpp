#pragma once

#include <array>
#include <asio.hpp>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>

#include <reply.hpp>
#include <req_handler.hpp>
#include <req_parser.hpp>
#include <request.hpp>

using asio::ip::tcp;
namespace htcpp {
class connection : public std::enable_shared_from_this<connection> {

public:
    using pointer = std::shared_ptr<connection>;

    static pointer create(asio::io_context& io_context, req_handler& handler)
    {
        return pointer(new connection(io_context, handler));
    }

    tcp::socket& socket() { return socket_; }

    void start();

private:
    tcp::socket socket_;
    htcpp::parser req_parser_;
    htcpp::req_handler& handler_;
    htcpp::request req_;
    std::array<char, 512> buffer_;

    connection(asio::io_context& io_context, req_handler& handler)
        : socket_(io_context)
        , handler_(handler)
        , buffer_()
    {
        // buffer_.reserve(512);
    }

    void do_read();
    void do_write(reply);
};
}
