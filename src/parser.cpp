#include <cctype>
// #include <iostream>
#include <req_parser.hpp>
#include <request.hpp>
#include <string>
#include <unordered_map>

using namespace htcpp;

namespace {
// table to ease the conversion between request string and their enum representation
static std::unordered_map<std::string, request::method> const table = {
    { "GET", request::method::get },
    { "HEAD", request::method::head },
    { "POST", request::method::post },
    { "OPTIONS", request::method::options },
    { "PUT", request::method::put }
};

}

htcpp::parse_result parser::consume(request& req, const char c)
{
    using namespace htcpp;
    // cctype functions are undefined if the char can't be represent as a unsigned char
    // so we need to cast it
    const auto ch = static_cast<unsigned char>(c);
    // std::cout << ch << std::endl;

    // big switch go brrrrrrrr
    switch (state_) {
    case state::method_begin: {
        if (!std::isalpha(ch) || std::islower(ch) || std::ispunct(ch) || std::iscntrl(ch) || std::isspace(ch)) {
            return parse_result::error;
        }

        state_ = state::method;
        buf_.push_back(c);
        return parse_result::undefined;
    }

    case state::method: {
        // here we finished consuming the method and just need to check if it's in the table
        if (ch == ' ') {
            auto search = table.find(buf_);

            if (search == table.end()) {
                return parse_result::error;
            }
            state_ = state::resource;
            buf_.clear();
            req.method_ = search->second;

            return parse_result::undefined;
        }

        if (std::isspace(ch) || std::islower(ch) || std::ispunct(ch) || std::iscntrl(ch)) {
            return parse_result::error;
        }

        buf_.push_back(c);
        return parse_result::undefined;
    }

    case state::resource: {

        if (ch == ' ') {
            state_ = state::version_h;
            return parse_result::undefined;
        }

        if (std::isspace(ch) || std::iscntrl(ch)) {
            return parse_result::error;
        }

        req.resource.push_back(c);
        return parse_result::undefined;
    }

    case state::version_h: {

        if (c == 'H') {
            state_ = state::version_t_1;
            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_t_1: {

        if (c == 'T') {
            state_ = state::version_t_2;
            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_t_2: {

        if (c == 'T') {
            state_ = state::version_p;
            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_p: {

        if (c == 'P') {
            state_ = state::version_slash;
            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_slash: {

        if (c == '/') {
            state_ = state::version_major;
            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_major: {

        if (c == '.') {
            state_ = state::version_minor;
            return parse_result::undefined;
        }

        if (std::isdigit(ch)) {
            req.major = c - '0';

            return parse_result::undefined;
        }

        return parse_result::error;
    }

    case state::version_minor: {

        if (c == '\r') {
            state_
                = state::expecting_newline_req;
            return parse_result::undefined;
        }

        if (std::isdigit(ch)) {
            req.minor = c - '0';

            return parse_result::undefined;
        }
        return parse_result::error;
    }

    case state::expecting_newline_req: {
        if (c == '\n') {
            state_ = state::map;
            return parse_result::undefined;
        }

        return parse_result::error;
    }
    case state::map: {
        return parse_result::undefined;
    }
    }

    return parse_result::undefined;
}
