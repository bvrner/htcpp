#include <reply.hpp>
#include <string_view>

using namespace htcpp;

// no constexpr std::string until C++20 F
namespace reply_status {

constexpr std::string_view ok("HTTP/1.0 200 OK\r\n");
constexpr std::string_view not_found("HTTP/1.0 404 Not Found\r\n");
constexpr std::string_view bad("HTTP/1.0 400 Bad Request\r\n");

asio::const_buffer buffer_from_status(reply::status status)
{
    switch (status) {
    case reply::status::ok:
        return asio::buffer(ok);
    case reply::status::not_found:
        return asio::buffer(not_found);
    default:
        return asio::buffer(bad);
    }
}
} // reply_status

namespace reply_bodies {

constexpr std::string_view ok("");
constexpr std::string_view not_found("<html>"
                                     "<head><title>Not found</title></head>"
                                     "<body>404 not found</body>"
                                     "</html>");
constexpr std::string_view bad("<html>"
                               "<head><title>Bad Request</title></head>"
                               "<body> 400 Bad Request</body>"
                               "</html>");

std::string body_from_status(reply::status status)
{
    switch (status) {
    case reply::status::ok:
        return std::string(ok);
    case reply::status::not_found:
        return std::string(not_found);
    default:
        return std::string(bad);
    }
}

} // reply bodies

reply reply::common_reply(reply::status status)
{
    reply ret;

    ret.status_ = status;
    ret.body = reply_bodies::body_from_status(status);
    ret.headers.resize(3);
    ret.headers[0].key = "Content-Type";
    ret.headers[0].value = "text/html";
    ret.headers[1].key = "Content-Length";
    ret.headers[1].value = std::to_string(ret.body.size());
    ret.headers[2].key = "Server";
    ret.headers[2].value = "htcpp";

    return ret;
}

[[nodiscard]] std::vector<asio::const_buffer> reply::to_buffers()
{
    constexpr std::string_view separator(": ");
    constexpr std::string_view crlf("\r\n");

    std::vector<asio::const_buffer> buffers;
    buffers.push_back(reply_status::buffer_from_status(status_));

    for (auto& header : headers) {
        buffers.push_back(asio::buffer(header.key));
        buffers.push_back(asio::buffer(separator));
        buffers.push_back(asio::buffer(header.value));
        buffers.push_back(asio::buffer(crlf));
    }
    buffers.push_back(asio::buffer(crlf));
    buffers.push_back(asio::buffer(body));
    return buffers;
}
