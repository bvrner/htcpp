#include <asio.hpp>
#include <server.hpp>

void htcpp::server::start_acceptor()
{
    connection::pointer new_connection = connection::create(io_context_, handler_);

    acceptor_.async_accept(new_connection->socket(),
        [this, new_connection](const asio::error_code& err) {
            if (!err) {
                new_connection->start();
            }
            start_acceptor();
        });
}
