#include <array>
#include <asio.hpp>
#include <connection.hpp>
#include <functional>
// #include <iostream>
#include <req_handler.hpp>
#include <request.hpp>
#include <sstream>

void htcpp::connection::do_read()
{
    // since we're working with async this function will return at once and drop
    // the connection unless we create a shared_ptr and pass it to the lambda
    // maintaing the object alive by being onwed by something
    auto self(shared_from_this());

    socket_.async_read_some(asio::buffer(buffer_),
        [this, self](const asio::error_code& e, [[maybe_unused]] std::size_t bytes) {
            if (!e) {

                auto result = req_parser_.parse(req_, buffer_.data(), buffer_.data() + bytes);

                using namespace htcpp;
                if (result == htcpp::parse_result::ok) {
                    auto response = handler_.handle(req_);

                    req_parser_.reset();
                    do_write(std::move(response));

                }

                else if (result == htcpp::parse_result::error) {
                    req_parser_.reset();
                    do_write(reply::common_reply(reply::status::bad));
                }

                else {
                    do_read();
                }
            }
        });
}

void htcpp::connection::do_write(reply message)
{
    // same as in do_read
    auto self(shared_from_this());

    asio::async_write(socket_, message.to_buffers(),
        [this, self](const asio::error_code& e, [[maybe_unused]] std::size_t bytes) {
            if (!e) {
                asio::error_code ignored;
                socket_.shutdown(asio::ip::tcp::socket::shutdown_both, ignored);
            }
        });
}

void htcpp::connection::start()
{
    do_read();
}
