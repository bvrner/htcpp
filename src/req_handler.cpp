#include <req_handler.hpp>

using namespace htcpp;
namespace fs = std::filesystem;

[[nodiscard]] reply req_handler::get(request& req)
{
    try {
        std::filesystem::path resource_path(root_);
        resource_path += req.resource;

        if (!resource_path.has_filename())
            resource_path /= "index.html";

        resource_path = std::filesystem::canonical(resource_path);

        std::string data;
        if (get_file_contents(resource_path, data)) {
            reply ret;

            ret.status_ = reply::status::ok;

            ret.headers.resize(3);
            ret.headers[0].key = "Server";
            ret.headers[0].value = "htcpp";

            ret.headers[1].key = "Content-Length";
            ret.headers[1].value = std::to_string(data.size());

            ret.headers[2].key = "Server";
            ret.headers[2].value = "htcpp";

            ret.body = std::move(data);

            return ret;
        } else {
            return reply::common_reply(reply::status::not_found);
        }
    } catch (...) {
        return reply::common_reply(reply::status::not_found);
    }
}

[[nodiscard]] reply req_handler::head(request& req)
{

    std::filesystem::path resource_path(root_);
    resource_path += req.resource;

    if (!resource_path.has_filename())
        resource_path /= "index.html";

    resource_path = std::filesystem::canonical(resource_path);

    if (std::filesystem::exists(resource_path)) {
        reply ret;

        ret.status_ = reply::status::ok;

        ret.headers.resize(3);
        ret.headers[0].key = "Server";
        ret.headers[0].value = "htcpp";

        ret.headers[1].key = "Content-Length";
        ret.headers[1].value = std::to_string(fs::file_size(resource_path));

        ret.headers[2].key = "Server";
        ret.headers[2].value = "htcpp";

        return ret;
    } else {
        return reply::common_reply(reply::status::not_found);
    }
}
