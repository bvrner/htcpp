#include <config.hpp>
#include <iostream>
#include <server.hpp>

int main(int argc, char** argv)
{
    try {
        htcpp::config config;

        if (argc > 1) {
            std::filesystem::path config_path(argv[1]);
            config = htcpp::config(config_path);
        }

        htcpp::server server(config);

        server.run(config.threads);

    } catch (std::exception& e) {
        std::cerr << e.what() << std::endl;
    }
    return 0;
}
