#include <map_parser.hpp>
#include <string>
#include <unordered_map>

htcpp::parse_result htcpp::map_parser::consume(std::unordered_map<std::string, std::string>& map, const char c)
{
    const auto ch = static_cast<unsigned char>(c);

    switch (state_) {
    case state::key_begin: {
        if (kind_ == kind::request && ch == '\r') {
            state_ = state::expect_newline_end;
            return htcpp::parse_result::undefined;
        }

        // we don't care about whitespace when parsing config files
        // but oh we do when parsing requests
        if (kind_ == kind::request && (std::isspace(ch) || std::iscntrl(ch)))
            return htcpp::parse_result::error;

        if (kind_ == kind::config && (std::isspace(ch) || std::iscntrl(ch)))
            return htcpp::parse_result::undefined;

        // if we reached here, all whitespace was skipped, so we can do this comparison without
        // worrying about messing empty lines in config files
        if (!std::isalpha(ch))
            return htcpp::parse_result::error;

        key_buf_.push_back(c);

        state_ = state::key;
        return htcpp::parse_result::undefined;
    }

    case state::key: {
        if (c == ' ') {
            state_ = state::colon;
            return htcpp::parse_result::undefined;
        }

        if (c == ':') {
            state_ = state::value_begin;
            return htcpp::parse_result::undefined;
        }

        if (std::iscntrl(ch) || std::isspace(ch))
            return htcpp::parse_result::error;

        key_buf_.push_back(c);
        return htcpp::parse_result::undefined;
    }
    case state::colon: {
        if (c == ':') {
            state_ = state::value_begin;
            return htcpp::parse_result::undefined;
        }

        if (std::isalpha(ch) || std::iscntrl(ch))
            return htcpp::parse_result::error;

        if (c == ' ')
            return htcpp::parse_result::undefined;

        return htcpp::parse_result::error;
    }

    case state::value_begin: {
        if (isspace(ch))
            return htcpp::parse_result::undefined;

        if (std::iscntrl(ch))
            return htcpp::parse_result::error;

        val_buf_.push_back(c);
        state_ = state::value;
        return htcpp::parse_result::undefined;
    }

    case state::value: {
        if (kind_ == kind::request && c == '\r') {
            state_ = state::expect_newline;
            return htcpp::parse_result::undefined;
        } else if (kind_ == kind::config && c == '\n') {
            state_ = state::key_begin;

            if (!key_buf_.empty()) {
                map[key_buf_] = val_buf_;

                key_buf_.clear();
                val_buf_.clear();
            }

            return htcpp::parse_result::undefined;
        }

        if (std::iscntrl(ch))
            return htcpp::parse_result::error;

        val_buf_.push_back(c);
        return htcpp::parse_result::undefined;
    }

    case state::expect_newline: {
        if (c == '\n') {
            if (!key_buf_.empty()) {
                map[key_buf_] = val_buf_;

                key_buf_.clear();
                val_buf_.clear();
            }
            state_ = state::key_begin;
            return htcpp::parse_result::undefined;
        } else {
            return htcpp::parse_result::error;
        }
    }
    case state::expect_newline_end: {
        if (c == '\n') {
            if (!key_buf_.empty()) {
                map[key_buf_] = val_buf_;

                key_buf_.clear();
                val_buf_.clear();
            }

            return htcpp::parse_result::ok;
        } else {
            return htcpp::parse_result::error;
        }
    }
    }

    return htcpp::parse_result::error;
}
